defmodule Brasileiro.FuncionariosTest do
  use Brasileiro.DataCase

  alias Brasileiro.Funcionarios

  describe "funcionarios" do
    alias Brasileiro.Funcionarios.Funcionario

    @valid_attrs %{contato: "some contato", nome: "some nome", profissao: "some profissao"}
    @update_attrs %{contato: "some updated contato", nome: "some updated nome", profissao: "some updated profissao"}
    @invalid_attrs %{contato: nil, nome: nil, profissao: nil}

    def funcionario_fixture(attrs \\ %{}) do
      {:ok, funcionario} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Funcionarios.create_funcionario()

      funcionario
    end

    test "list_funcionarios/0 returns all funcionarios" do
      funcionario = funcionario_fixture()
      assert Funcionarios.list_funcionarios() == [funcionario]
    end

    test "get_funcionario!/1 returns the funcionario with given id" do
      funcionario = funcionario_fixture()
      assert Funcionarios.get_funcionario!(funcionario.id) == funcionario
    end

    test "create_funcionario/1 with valid data creates a funcionario" do
      assert {:ok, %Funcionario{} = funcionario} = Funcionarios.create_funcionario(@valid_attrs)
      assert funcionario.contato == "some contato"
      assert funcionario.nome == "some nome"
      assert funcionario.profissao == "some profissao"
    end

    test "create_funcionario/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Funcionarios.create_funcionario(@invalid_attrs)
    end

    test "update_funcionario/2 with valid data updates the funcionario" do
      funcionario = funcionario_fixture()
      assert {:ok, funcionario} = Funcionarios.update_funcionario(funcionario, @update_attrs)
      assert %Funcionario{} = funcionario
      assert funcionario.contato == "some updated contato"
      assert funcionario.nome == "some updated nome"
      assert funcionario.profissao == "some updated profissao"
    end

    test "update_funcionario/2 with invalid data returns error changeset" do
      funcionario = funcionario_fixture()
      assert {:error, %Ecto.Changeset{}} = Funcionarios.update_funcionario(funcionario, @invalid_attrs)
      assert funcionario == Funcionarios.get_funcionario!(funcionario.id)
    end

    test "delete_funcionario/1 deletes the funcionario" do
      funcionario = funcionario_fixture()
      assert {:ok, %Funcionario{}} = Funcionarios.delete_funcionario(funcionario)
      assert_raise Ecto.NoResultsError, fn -> Funcionarios.get_funcionario!(funcionario.id) end
    end

    test "change_funcionario/1 returns a funcionario changeset" do
      funcionario = funcionario_fixture()
      assert %Ecto.Changeset{} = Funcionarios.change_funcionario(funcionario)
    end
  end
end
