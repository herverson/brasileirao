defmodule Brasileiro.CompeticoesTest do
  use Brasileiro.DataCase

  alias Brasileiro.Competicoes

  describe "competicoes" do
    alias Brasileiro.Competicoes.Competicao

    @valid_attrs %{id_jogo: 42, id_time: 42, nome: "some nome"}
    @update_attrs %{id_jogo: 43, id_time: 43, nome: "some updated nome"}
    @invalid_attrs %{id_jogo: nil, id_time: nil, nome: nil}

    def competicao_fixture(attrs \\ %{}) do
      {:ok, competicao} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Competicoes.create_competicao()

      competicao
    end

    test "list_competicoes/0 returns all competicoes" do
      competicao = competicao_fixture()
      assert Competicoes.list_competicoes() == [competicao]
    end

    test "get_competicao!/1 returns the competicao with given id" do
      competicao = competicao_fixture()
      assert Competicoes.get_competicao!(competicao.id) == competicao
    end

    test "create_competicao/1 with valid data creates a competicao" do
      assert {:ok, %Competicao{} = competicao} = Competicoes.create_competicao(@valid_attrs)
      assert competicao.id_jogo == 42
      assert competicao.id_time == 42
      assert competicao.nome == "some nome"
    end

    test "create_competicao/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Competicoes.create_competicao(@invalid_attrs)
    end

    test "update_competicao/2 with valid data updates the competicao" do
      competicao = competicao_fixture()
      assert {:ok, competicao} = Competicoes.update_competicao(competicao, @update_attrs)
      assert %Competicao{} = competicao
      assert competicao.id_jogo == 43
      assert competicao.id_time == 43
      assert competicao.nome == "some updated nome"
    end

    test "update_competicao/2 with invalid data returns error changeset" do
      competicao = competicao_fixture()
      assert {:error, %Ecto.Changeset{}} = Competicoes.update_competicao(competicao, @invalid_attrs)
      assert competicao == Competicoes.get_competicao!(competicao.id)
    end

    test "delete_competicao/1 deletes the competicao" do
      competicao = competicao_fixture()
      assert {:ok, %Competicao{}} = Competicoes.delete_competicao(competicao)
      assert_raise Ecto.NoResultsError, fn -> Competicoes.get_competicao!(competicao.id) end
    end

    test "change_competicao/1 returns a competicao changeset" do
      competicao = competicao_fixture()
      assert %Ecto.Changeset{} = Competicoes.change_competicao(competicao)
    end
  end
end
