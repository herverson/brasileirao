defmodule Brasileiro.JogosTest do
  use Brasileiro.DataCase

  alias Brasileiro.Jogos

  describe "jogos" do
    alias Brasileiro.Jogos.Jogo

    @valid_attrs %{comissao_tecnica: 42, data: ~N[2010-04-17 14:00:00.000000], id_escalacao_casa: 42, id_escalacao_visitante: 42, id_resultado: 42, local: "some local", sumula: 42}
    @update_attrs %{comissao_tecnica: 43, data: ~N[2011-05-18 15:01:01.000000], id_escalacao_casa: 43, id_escalacao_visitante: 43, id_resultado: 43, local: "some updated local", sumula: 43}
    @invalid_attrs %{comissao_tecnica: nil, data: nil, id_escalacao_casa: nil, id_escalacao_visitante: nil, id_resultado: nil, local: nil, sumula: nil}

    def jogo_fixture(attrs \\ %{}) do
      {:ok, jogo} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Jogos.create_jogo()

      jogo
    end

    test "list_jogos/0 returns all jogos" do
      jogo = jogo_fixture()
      assert Jogos.list_jogos() == [jogo]
    end

    test "get_jogo!/1 returns the jogo with given id" do
      jogo = jogo_fixture()
      assert Jogos.get_jogo!(jogo.id) == jogo
    end

    test "create_jogo/1 with valid data creates a jogo" do
      assert {:ok, %Jogo{} = jogo} = Jogos.create_jogo(@valid_attrs)
      assert jogo.comissao_tecnica == 42
      assert jogo.data == ~N[2010-04-17 14:00:00.000000]
      assert jogo.id_escalacao_casa == 42
      assert jogo.id_escalacao_visitante == 42
      assert jogo.id_resultado == 42
      assert jogo.local == "some local"
      assert jogo.sumula == 42
    end

    test "create_jogo/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Jogos.create_jogo(@invalid_attrs)
    end

    test "update_jogo/2 with valid data updates the jogo" do
      jogo = jogo_fixture()
      assert {:ok, jogo} = Jogos.update_jogo(jogo, @update_attrs)
      assert %Jogo{} = jogo
      assert jogo.comissao_tecnica == 43
      assert jogo.data == ~N[2011-05-18 15:01:01.000000]
      assert jogo.id_escalacao_casa == 43
      assert jogo.id_escalacao_visitante == 43
      assert jogo.id_resultado == 43
      assert jogo.local == "some updated local"
      assert jogo.sumula == 43
    end

    test "update_jogo/2 with invalid data returns error changeset" do
      jogo = jogo_fixture()
      assert {:error, %Ecto.Changeset{}} = Jogos.update_jogo(jogo, @invalid_attrs)
      assert jogo == Jogos.get_jogo!(jogo.id)
    end

    test "delete_jogo/1 deletes the jogo" do
      jogo = jogo_fixture()
      assert {:ok, %Jogo{}} = Jogos.delete_jogo(jogo)
      assert_raise Ecto.NoResultsError, fn -> Jogos.get_jogo!(jogo.id) end
    end

    test "change_jogo/1 returns a jogo changeset" do
      jogo = jogo_fixture()
      assert %Ecto.Changeset{} = Jogos.change_jogo(jogo)
    end
  end
end
