defmodule Brasileiro.SumulasTest do
  use Brasileiro.DataCase

  alias Brasileiro.Sumulas

  describe "sumulas" do
    alias Brasileiro.Sumulas.Sumula

    @valid_attrs %{id_jogador_amarelo: 42, id_jogador_vermelho: 42, relatorio: "some relatorio"}
    @update_attrs %{id_jogador_amarelo: 43, id_jogador_vermelho: 43, relatorio: "some updated relatorio"}
    @invalid_attrs %{id_jogador_amarelo: nil, id_jogador_vermelho: nil, relatorio: nil}

    def sumula_fixture(attrs \\ %{}) do
      {:ok, sumula} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Sumulas.create_sumula()

      sumula
    end

    test "list_sumulas/0 returns all sumulas" do
      sumula = sumula_fixture()
      assert Sumulas.list_sumulas() == [sumula]
    end

    test "get_sumula!/1 returns the sumula with given id" do
      sumula = sumula_fixture()
      assert Sumulas.get_sumula!(sumula.id) == sumula
    end

    test "create_sumula/1 with valid data creates a sumula" do
      assert {:ok, %Sumula{} = sumula} = Sumulas.create_sumula(@valid_attrs)
      assert sumula.id_jogador_amarelo == 42
      assert sumula.id_jogador_vermelho == 42
      assert sumula.relatorio == "some relatorio"
    end

    test "create_sumula/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sumulas.create_sumula(@invalid_attrs)
    end

    test "update_sumula/2 with valid data updates the sumula" do
      sumula = sumula_fixture()
      assert {:ok, sumula} = Sumulas.update_sumula(sumula, @update_attrs)
      assert %Sumula{} = sumula
      assert sumula.id_jogador_amarelo == 43
      assert sumula.id_jogador_vermelho == 43
      assert sumula.relatorio == "some updated relatorio"
    end

    test "update_sumula/2 with invalid data returns error changeset" do
      sumula = sumula_fixture()
      assert {:error, %Ecto.Changeset{}} = Sumulas.update_sumula(sumula, @invalid_attrs)
      assert sumula == Sumulas.get_sumula!(sumula.id)
    end

    test "delete_sumula/1 deletes the sumula" do
      sumula = sumula_fixture()
      assert {:ok, %Sumula{}} = Sumulas.delete_sumula(sumula)
      assert_raise Ecto.NoResultsError, fn -> Sumulas.get_sumula!(sumula.id) end
    end

    test "change_sumula/1 returns a sumula changeset" do
      sumula = sumula_fixture()
      assert %Ecto.Changeset{} = Sumulas.change_sumula(sumula)
    end
  end
end
