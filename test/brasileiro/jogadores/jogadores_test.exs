defmodule Brasileiro.JogadoresTest do
  use Brasileiro.DataCase

  alias Brasileiro.Jogadores

  describe "jogadores" do
    alias Brasileiro.Jogadores.Jogador

    @valid_attrs %{nome: "some nome", numero_camisa: "some numero_camisa", posicao: "some posicao"}
    @update_attrs %{nome: "some updated nome", numero_camisa: "some updated numero_camisa", posicao: "some updated posicao"}
    @invalid_attrs %{nome: nil, numero_camisa: nil, posicao: nil}

    def jogador_fixture(attrs \\ %{}) do
      {:ok, jogador} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Jogadores.create_jogador()

      jogador
    end

    test "list_jogadores/0 returns all jogadores" do
      jogador = jogador_fixture()
      assert Jogadores.list_jogadores() == [jogador]
    end

    test "get_jogador!/1 returns the jogador with given id" do
      jogador = jogador_fixture()
      assert Jogadores.get_jogador!(jogador.id) == jogador
    end

    test "create_jogador/1 with valid data creates a jogador" do
      assert {:ok, %Jogador{} = jogador} = Jogadores.create_jogador(@valid_attrs)
      assert jogador.nome == "some nome"
      assert jogador.numero_camisa == "some numero_camisa"
      assert jogador.posicao == "some posicao"
    end

    test "create_jogador/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Jogadores.create_jogador(@invalid_attrs)
    end

    test "update_jogador/2 with valid data updates the jogador" do
      jogador = jogador_fixture()
      assert {:ok, jogador} = Jogadores.update_jogador(jogador, @update_attrs)
      assert %Jogador{} = jogador
      assert jogador.nome == "some updated nome"
      assert jogador.numero_camisa == "some updated numero_camisa"
      assert jogador.posicao == "some updated posicao"
    end

    test "update_jogador/2 with invalid data returns error changeset" do
      jogador = jogador_fixture()
      assert {:error, %Ecto.Changeset{}} = Jogadores.update_jogador(jogador, @invalid_attrs)
      assert jogador == Jogadores.get_jogador!(jogador.id)
    end

    test "delete_jogador/1 deletes the jogador" do
      jogador = jogador_fixture()
      assert {:ok, %Jogador{}} = Jogadores.delete_jogador(jogador)
      assert_raise Ecto.NoResultsError, fn -> Jogadores.get_jogador!(jogador.id) end
    end

    test "change_jogador/1 returns a jogador changeset" do
      jogador = jogador_fixture()
      assert %Ecto.Changeset{} = Jogadores.change_jogador(jogador)
    end
  end
end
