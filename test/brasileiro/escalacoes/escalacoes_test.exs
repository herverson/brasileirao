defmodule Brasileiro.EscalacoesTest do
  use Brasileiro.DataCase

  alias Brasileiro.Escalacoes

  describe "escalacoes" do
    alias Brasileiro.Escalacoes.Escalacao

    @valid_attrs %{jogadores_principais: 42, jogadores_reservas: 42, tecnico: 42}
    @update_attrs %{jogadores_principais: 43, jogadores_reservas: 43, tecnico: 43}
    @invalid_attrs %{jogadores_principais: nil, jogadores_reservas: nil, tecnico: nil}

    def escalacao_fixture(attrs \\ %{}) do
      {:ok, escalacao} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Escalacoes.create_escalacao()

      escalacao
    end

    test "list_escalacoes/0 returns all escalacoes" do
      escalacao = escalacao_fixture()
      assert Escalacoes.list_escalacoes() == [escalacao]
    end

    test "get_escalacao!/1 returns the escalacao with given id" do
      escalacao = escalacao_fixture()
      assert Escalacoes.get_escalacao!(escalacao.id) == escalacao
    end

    test "create_escalacao/1 with valid data creates a escalacao" do
      assert {:ok, %Escalacao{} = escalacao} = Escalacoes.create_escalacao(@valid_attrs)
      assert escalacao.jogadores_principais == 42
      assert escalacao.jogadores_reservas == 42
      assert escalacao.tecnico == 42
    end

    test "create_escalacao/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Escalacoes.create_escalacao(@invalid_attrs)
    end

    test "update_escalacao/2 with valid data updates the escalacao" do
      escalacao = escalacao_fixture()
      assert {:ok, escalacao} = Escalacoes.update_escalacao(escalacao, @update_attrs)
      assert %Escalacao{} = escalacao
      assert escalacao.jogadores_principais == 43
      assert escalacao.jogadores_reservas == 43
      assert escalacao.tecnico == 43
    end

    test "update_escalacao/2 with invalid data returns error changeset" do
      escalacao = escalacao_fixture()
      assert {:error, %Ecto.Changeset{}} = Escalacoes.update_escalacao(escalacao, @invalid_attrs)
      assert escalacao == Escalacoes.get_escalacao!(escalacao.id)
    end

    test "delete_escalacao/1 deletes the escalacao" do
      escalacao = escalacao_fixture()
      assert {:ok, %Escalacao{}} = Escalacoes.delete_escalacao(escalacao)
      assert_raise Ecto.NoResultsError, fn -> Escalacoes.get_escalacao!(escalacao.id) end
    end

    test "change_escalacao/1 returns a escalacao changeset" do
      escalacao = escalacao_fixture()
      assert %Ecto.Changeset{} = Escalacoes.change_escalacao(escalacao)
    end
  end

  describe "escalacoes" do
    alias Brasileiro.Escalacoes.Escalacao

    @valid_attrs %{id_time: 42, jogadores_principais: 42, jogadores_reservas: 42, tecnico: 42}
    @update_attrs %{id_time: 43, jogadores_principais: 43, jogadores_reservas: 43, tecnico: 43}
    @invalid_attrs %{id_time: nil, jogadores_principais: nil, jogadores_reservas: nil, tecnico: nil}

    def escalacao_fixture(attrs \\ %{}) do
      {:ok, escalacao} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Escalacoes.create_escalacao()

      escalacao
    end

    test "list_escalacoes/0 returns all escalacoes" do
      escalacao = escalacao_fixture()
      assert Escalacoes.list_escalacoes() == [escalacao]
    end

    test "get_escalacao!/1 returns the escalacao with given id" do
      escalacao = escalacao_fixture()
      assert Escalacoes.get_escalacao!(escalacao.id) == escalacao
    end

    test "create_escalacao/1 with valid data creates a escalacao" do
      assert {:ok, %Escalacao{} = escalacao} = Escalacoes.create_escalacao(@valid_attrs)
      assert escalacao.id_time == 42
      assert escalacao.jogadores_principais == 42
      assert escalacao.jogadores_reservas == 42
      assert escalacao.tecnico == 42
    end

    test "create_escalacao/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Escalacoes.create_escalacao(@invalid_attrs)
    end

    test "update_escalacao/2 with valid data updates the escalacao" do
      escalacao = escalacao_fixture()
      assert {:ok, escalacao} = Escalacoes.update_escalacao(escalacao, @update_attrs)
      assert %Escalacao{} = escalacao
      assert escalacao.id_time == 43
      assert escalacao.jogadores_principais == 43
      assert escalacao.jogadores_reservas == 43
      assert escalacao.tecnico == 43
    end

    test "update_escalacao/2 with invalid data returns error changeset" do
      escalacao = escalacao_fixture()
      assert {:error, %Ecto.Changeset{}} = Escalacoes.update_escalacao(escalacao, @invalid_attrs)
      assert escalacao == Escalacoes.get_escalacao!(escalacao.id)
    end

    test "delete_escalacao/1 deletes the escalacao" do
      escalacao = escalacao_fixture()
      assert {:ok, %Escalacao{}} = Escalacoes.delete_escalacao(escalacao)
      assert_raise Ecto.NoResultsError, fn -> Escalacoes.get_escalacao!(escalacao.id) end
    end

    test "change_escalacao/1 returns a escalacao changeset" do
      escalacao = escalacao_fixture()
      assert %Ecto.Changeset{} = Escalacoes.change_escalacao(escalacao)
    end
  end
end
