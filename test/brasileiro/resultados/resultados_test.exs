defmodule Brasileiro.ResultadosTest do
  use Brasileiro.DataCase

  alias Brasileiro.Resultados

  describe "resultados" do
    alias Brasileiro.Resultados.Resultado

    @valid_attrs %{gols_casa: 42, gols_visitante: 42, id_perdedor: 42, id_vencedor: 42}
    @update_attrs %{gols_casa: 43, gols_visitante: 43, id_perdedor: 43, id_vencedor: 43}
    @invalid_attrs %{gols_casa: nil, gols_visitante: nil, id_perdedor: nil, id_vencedor: nil}

    def resultado_fixture(attrs \\ %{}) do
      {:ok, resultado} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Resultados.create_resultado()

      resultado
    end

    test "list_resultados/0 returns all resultados" do
      resultado = resultado_fixture()
      assert Resultados.list_resultados() == [resultado]
    end

    test "get_resultado!/1 returns the resultado with given id" do
      resultado = resultado_fixture()
      assert Resultados.get_resultado!(resultado.id) == resultado
    end

    test "create_resultado/1 with valid data creates a resultado" do
      assert {:ok, %Resultado{} = resultado} = Resultados.create_resultado(@valid_attrs)
      assert resultado.gols_casa == 42
      assert resultado.gols_visitante == 42
      assert resultado.id_perdedor == 42
      assert resultado.id_vencedor == 42
    end

    test "create_resultado/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Resultados.create_resultado(@invalid_attrs)
    end

    test "update_resultado/2 with valid data updates the resultado" do
      resultado = resultado_fixture()
      assert {:ok, resultado} = Resultados.update_resultado(resultado, @update_attrs)
      assert %Resultado{} = resultado
      assert resultado.gols_casa == 43
      assert resultado.gols_visitante == 43
      assert resultado.id_perdedor == 43
      assert resultado.id_vencedor == 43
    end

    test "update_resultado/2 with invalid data returns error changeset" do
      resultado = resultado_fixture()
      assert {:error, %Ecto.Changeset{}} = Resultados.update_resultado(resultado, @invalid_attrs)
      assert resultado == Resultados.get_resultado!(resultado.id)
    end

    test "delete_resultado/1 deletes the resultado" do
      resultado = resultado_fixture()
      assert {:ok, %Resultado{}} = Resultados.delete_resultado(resultado)
      assert_raise Ecto.NoResultsError, fn -> Resultados.get_resultado!(resultado.id) end
    end

    test "change_resultado/1 returns a resultado changeset" do
      resultado = resultado_fixture()
      assert %Ecto.Changeset{} = Resultados.change_resultado(resultado)
    end
  end
end
