defmodule BrasileiroWeb.JogadorControllerTest do
  use BrasileiroWeb.ConnCase

  alias Brasileiro.Jogadores

  @create_attrs %{nome: "some nome", numero_camisa: "some numero_camisa", posicao: "some posicao"}
  @update_attrs %{nome: "some updated nome", numero_camisa: "some updated numero_camisa", posicao: "some updated posicao"}
  @invalid_attrs %{nome: nil, numero_camisa: nil, posicao: nil}

  def fixture(:jogador) do
    {:ok, jogador} = Jogadores.create_jogador(@create_attrs)
    jogador
  end

  describe "index" do
    test "lists all jogadores", %{conn: conn} do
      conn = get conn, jogador_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Jogadores"
    end
  end

  describe "new jogador" do
    test "renders form", %{conn: conn} do
      conn = get conn, jogador_path(conn, :new)
      assert html_response(conn, 200) =~ "New Jogador"
    end
  end

  describe "create jogador" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, jogador_path(conn, :create), jogador: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == jogador_path(conn, :show, id)

      conn = get conn, jogador_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Jogador"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, jogador_path(conn, :create), jogador: @invalid_attrs
      assert html_response(conn, 200) =~ "New Jogador"
    end
  end

  describe "edit jogador" do
    setup [:create_jogador]

    test "renders form for editing chosen jogador", %{conn: conn, jogador: jogador} do
      conn = get conn, jogador_path(conn, :edit, jogador)
      assert html_response(conn, 200) =~ "Edit Jogador"
    end
  end

  describe "update jogador" do
    setup [:create_jogador]

    test "redirects when data is valid", %{conn: conn, jogador: jogador} do
      conn = put conn, jogador_path(conn, :update, jogador), jogador: @update_attrs
      assert redirected_to(conn) == jogador_path(conn, :show, jogador)

      conn = get conn, jogador_path(conn, :show, jogador)
      assert html_response(conn, 200) =~ "some updated nome"
    end

    test "renders errors when data is invalid", %{conn: conn, jogador: jogador} do
      conn = put conn, jogador_path(conn, :update, jogador), jogador: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Jogador"
    end
  end

  describe "delete jogador" do
    setup [:create_jogador]

    test "deletes chosen jogador", %{conn: conn, jogador: jogador} do
      conn = delete conn, jogador_path(conn, :delete, jogador)
      assert redirected_to(conn) == jogador_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, jogador_path(conn, :show, jogador)
      end
    end
  end

  defp create_jogador(_) do
    jogador = fixture(:jogador)
    {:ok, jogador: jogador}
  end
end
