defmodule BrasileiroWeb.JogoControllerTest do
  use BrasileiroWeb.ConnCase

  alias Brasileiro.Jogos

  @create_attrs %{comissao_tecnica: 42, data: ~N[2010-04-17 14:00:00.000000], id_escalacao_casa: 42, id_escalacao_visitante: 42, id_resultado: 42, local: "some local", sumula: 42}
  @update_attrs %{comissao_tecnica: 43, data: ~N[2011-05-18 15:01:01.000000], id_escalacao_casa: 43, id_escalacao_visitante: 43, id_resultado: 43, local: "some updated local", sumula: 43}
  @invalid_attrs %{comissao_tecnica: nil, data: nil, id_escalacao_casa: nil, id_escalacao_visitante: nil, id_resultado: nil, local: nil, sumula: nil}

  def fixture(:jogo) do
    {:ok, jogo} = Jogos.create_jogo(@create_attrs)
    jogo
  end

  describe "index" do
    test "lists all jogos", %{conn: conn} do
      conn = get conn, jogo_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Jogos"
    end
  end

  describe "new jogo" do
    test "renders form", %{conn: conn} do
      conn = get conn, jogo_path(conn, :new)
      assert html_response(conn, 200) =~ "New Jogo"
    end
  end

  describe "create jogo" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, jogo_path(conn, :create), jogo: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == jogo_path(conn, :show, id)

      conn = get conn, jogo_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Jogo"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, jogo_path(conn, :create), jogo: @invalid_attrs
      assert html_response(conn, 200) =~ "New Jogo"
    end
  end

  describe "edit jogo" do
    setup [:create_jogo]

    test "renders form for editing chosen jogo", %{conn: conn, jogo: jogo} do
      conn = get conn, jogo_path(conn, :edit, jogo)
      assert html_response(conn, 200) =~ "Edit Jogo"
    end
  end

  describe "update jogo" do
    setup [:create_jogo]

    test "redirects when data is valid", %{conn: conn, jogo: jogo} do
      conn = put conn, jogo_path(conn, :update, jogo), jogo: @update_attrs
      assert redirected_to(conn) == jogo_path(conn, :show, jogo)

      conn = get conn, jogo_path(conn, :show, jogo)
      assert html_response(conn, 200) =~ "some updated local"
    end

    test "renders errors when data is invalid", %{conn: conn, jogo: jogo} do
      conn = put conn, jogo_path(conn, :update, jogo), jogo: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Jogo"
    end
  end

  describe "delete jogo" do
    setup [:create_jogo]

    test "deletes chosen jogo", %{conn: conn, jogo: jogo} do
      conn = delete conn, jogo_path(conn, :delete, jogo)
      assert redirected_to(conn) == jogo_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, jogo_path(conn, :show, jogo)
      end
    end
  end

  defp create_jogo(_) do
    jogo = fixture(:jogo)
    {:ok, jogo: jogo}
  end
end
