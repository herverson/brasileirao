defmodule BrasileiroWeb.CompeticaoControllerTest do
  use BrasileiroWeb.ConnCase

  alias Brasileiro.Competicoes

  @create_attrs %{id_jogo: 42, id_time: 42, nome: "some nome"}
  @update_attrs %{id_jogo: 43, id_time: 43, nome: "some updated nome"}
  @invalid_attrs %{id_jogo: nil, id_time: nil, nome: nil}

  def fixture(:competicao) do
    {:ok, competicao} = Competicoes.create_competicao(@create_attrs)
    competicao
  end

  describe "index" do
    test "lists all competicoes", %{conn: conn} do
      conn = get conn, competicao_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Competicoes"
    end
  end

  describe "new competicao" do
    test "renders form", %{conn: conn} do
      conn = get conn, competicao_path(conn, :new)
      assert html_response(conn, 200) =~ "New Competicao"
    end
  end

  describe "create competicao" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, competicao_path(conn, :create), competicao: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == competicao_path(conn, :show, id)

      conn = get conn, competicao_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Competicao"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, competicao_path(conn, :create), competicao: @invalid_attrs
      assert html_response(conn, 200) =~ "New Competicao"
    end
  end

  describe "edit competicao" do
    setup [:create_competicao]

    test "renders form for editing chosen competicao", %{conn: conn, competicao: competicao} do
      conn = get conn, competicao_path(conn, :edit, competicao)
      assert html_response(conn, 200) =~ "Edit Competicao"
    end
  end

  describe "update competicao" do
    setup [:create_competicao]

    test "redirects when data is valid", %{conn: conn, competicao: competicao} do
      conn = put conn, competicao_path(conn, :update, competicao), competicao: @update_attrs
      assert redirected_to(conn) == competicao_path(conn, :show, competicao)

      conn = get conn, competicao_path(conn, :show, competicao)
      assert html_response(conn, 200) =~ "some updated nome"
    end

    test "renders errors when data is invalid", %{conn: conn, competicao: competicao} do
      conn = put conn, competicao_path(conn, :update, competicao), competicao: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Competicao"
    end
  end

  describe "delete competicao" do
    setup [:create_competicao]

    test "deletes chosen competicao", %{conn: conn, competicao: competicao} do
      conn = delete conn, competicao_path(conn, :delete, competicao)
      assert redirected_to(conn) == competicao_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, competicao_path(conn, :show, competicao)
      end
    end
  end

  defp create_competicao(_) do
    competicao = fixture(:competicao)
    {:ok, competicao: competicao}
  end
end
