defmodule BrasileiroWeb.EscalacaoControllerTest do
  use BrasileiroWeb.ConnCase

  alias Brasileiro.Escalacoes

  @create_attrs %{id_time: 42, jogadores_principais: 42, jogadores_reservas: 42, tecnico: 42}
  @update_attrs %{id_time: 43, jogadores_principais: 43, jogadores_reservas: 43, tecnico: 43}
  @invalid_attrs %{id_time: nil, jogadores_principais: nil, jogadores_reservas: nil, tecnico: nil}

  def fixture(:escalacao) do
    {:ok, escalacao} = Escalacoes.create_escalacao(@create_attrs)
    escalacao
  end

  describe "index" do
    test "lists all escalacoes", %{conn: conn} do
      conn = get conn, escalacao_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Escalacoes"
    end
  end

  describe "new escalacao" do
    test "renders form", %{conn: conn} do
      conn = get conn, escalacao_path(conn, :new)
      assert html_response(conn, 200) =~ "New Escalacao"
    end
  end

  describe "create escalacao" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, escalacao_path(conn, :create), escalacao: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == escalacao_path(conn, :show, id)

      conn = get conn, escalacao_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Escalacao"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, escalacao_path(conn, :create), escalacao: @invalid_attrs
      assert html_response(conn, 200) =~ "New Escalacao"
    end
  end

  describe "edit escalacao" do
    setup [:create_escalacao]

    test "renders form for editing chosen escalacao", %{conn: conn, escalacao: escalacao} do
      conn = get conn, escalacao_path(conn, :edit, escalacao)
      assert html_response(conn, 200) =~ "Edit Escalacao"
    end
  end

  describe "update escalacao" do
    setup [:create_escalacao]

    test "redirects when data is valid", %{conn: conn, escalacao: escalacao} do
      conn = put conn, escalacao_path(conn, :update, escalacao), escalacao: @update_attrs
      assert redirected_to(conn) == escalacao_path(conn, :show, escalacao)

      conn = get conn, escalacao_path(conn, :show, escalacao)
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, escalacao: escalacao} do
      conn = put conn, escalacao_path(conn, :update, escalacao), escalacao: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Escalacao"
    end
  end

  describe "delete escalacao" do
    setup [:create_escalacao]

    test "deletes chosen escalacao", %{conn: conn, escalacao: escalacao} do
      conn = delete conn, escalacao_path(conn, :delete, escalacao)
      assert redirected_to(conn) == escalacao_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, escalacao_path(conn, :show, escalacao)
      end
    end
  end

  defp create_escalacao(_) do
    escalacao = fixture(:escalacao)
    {:ok, escalacao: escalacao}
  end
end
