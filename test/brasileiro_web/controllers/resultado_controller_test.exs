defmodule BrasileiroWeb.ResultadoControllerTest do
  use BrasileiroWeb.ConnCase

  alias Brasileiro.Resultados

  @create_attrs %{gols_casa: 42, gols_visitante: 42, id_perdedor: 42, id_vencedor: 42}
  @update_attrs %{gols_casa: 43, gols_visitante: 43, id_perdedor: 43, id_vencedor: 43}
  @invalid_attrs %{gols_casa: nil, gols_visitante: nil, id_perdedor: nil, id_vencedor: nil}

  def fixture(:resultado) do
    {:ok, resultado} = Resultados.create_resultado(@create_attrs)
    resultado
  end

  describe "index" do
    test "lists all resultados", %{conn: conn} do
      conn = get conn, resultado_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Resultados"
    end
  end

  describe "new resultado" do
    test "renders form", %{conn: conn} do
      conn = get conn, resultado_path(conn, :new)
      assert html_response(conn, 200) =~ "New Resultado"
    end
  end

  describe "create resultado" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, resultado_path(conn, :create), resultado: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == resultado_path(conn, :show, id)

      conn = get conn, resultado_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Resultado"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, resultado_path(conn, :create), resultado: @invalid_attrs
      assert html_response(conn, 200) =~ "New Resultado"
    end
  end

  describe "edit resultado" do
    setup [:create_resultado]

    test "renders form for editing chosen resultado", %{conn: conn, resultado: resultado} do
      conn = get conn, resultado_path(conn, :edit, resultado)
      assert html_response(conn, 200) =~ "Edit Resultado"
    end
  end

  describe "update resultado" do
    setup [:create_resultado]

    test "redirects when data is valid", %{conn: conn, resultado: resultado} do
      conn = put conn, resultado_path(conn, :update, resultado), resultado: @update_attrs
      assert redirected_to(conn) == resultado_path(conn, :show, resultado)

      conn = get conn, resultado_path(conn, :show, resultado)
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, resultado: resultado} do
      conn = put conn, resultado_path(conn, :update, resultado), resultado: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Resultado"
    end
  end

  describe "delete resultado" do
    setup [:create_resultado]

    test "deletes chosen resultado", %{conn: conn, resultado: resultado} do
      conn = delete conn, resultado_path(conn, :delete, resultado)
      assert redirected_to(conn) == resultado_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, resultado_path(conn, :show, resultado)
      end
    end
  end

  defp create_resultado(_) do
    resultado = fixture(:resultado)
    {:ok, resultado: resultado}
  end
end
