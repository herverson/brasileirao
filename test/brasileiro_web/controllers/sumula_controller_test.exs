defmodule BrasileiroWeb.SumulaControllerTest do
  use BrasileiroWeb.ConnCase

  alias Brasileiro.Sumulas

  @create_attrs %{id_jogador_amarelo: 42, id_jogador_vermelho: 42, relatorio: "some relatorio"}
  @update_attrs %{id_jogador_amarelo: 43, id_jogador_vermelho: 43, relatorio: "some updated relatorio"}
  @invalid_attrs %{id_jogador_amarelo: nil, id_jogador_vermelho: nil, relatorio: nil}

  def fixture(:sumula) do
    {:ok, sumula} = Sumulas.create_sumula(@create_attrs)
    sumula
  end

  describe "index" do
    test "lists all sumulas", %{conn: conn} do
      conn = get conn, sumula_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Sumulas"
    end
  end

  describe "new sumula" do
    test "renders form", %{conn: conn} do
      conn = get conn, sumula_path(conn, :new)
      assert html_response(conn, 200) =~ "New Sumula"
    end
  end

  describe "create sumula" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, sumula_path(conn, :create), sumula: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == sumula_path(conn, :show, id)

      conn = get conn, sumula_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Sumula"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, sumula_path(conn, :create), sumula: @invalid_attrs
      assert html_response(conn, 200) =~ "New Sumula"
    end
  end

  describe "edit sumula" do
    setup [:create_sumula]

    test "renders form for editing chosen sumula", %{conn: conn, sumula: sumula} do
      conn = get conn, sumula_path(conn, :edit, sumula)
      assert html_response(conn, 200) =~ "Edit Sumula"
    end
  end

  describe "update sumula" do
    setup [:create_sumula]

    test "redirects when data is valid", %{conn: conn, sumula: sumula} do
      conn = put conn, sumula_path(conn, :update, sumula), sumula: @update_attrs
      assert redirected_to(conn) == sumula_path(conn, :show, sumula)

      conn = get conn, sumula_path(conn, :show, sumula)
      assert html_response(conn, 200) =~ "some updated relatorio"
    end

    test "renders errors when data is invalid", %{conn: conn, sumula: sumula} do
      conn = put conn, sumula_path(conn, :update, sumula), sumula: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Sumula"
    end
  end

  describe "delete sumula" do
    setup [:create_sumula]

    test "deletes chosen sumula", %{conn: conn, sumula: sumula} do
      conn = delete conn, sumula_path(conn, :delete, sumula)
      assert redirected_to(conn) == sumula_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, sumula_path(conn, :show, sumula)
      end
    end
  end

  defp create_sumula(_) do
    sumula = fixture(:sumula)
    {:ok, sumula: sumula}
  end
end
