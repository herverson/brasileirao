# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :brasileiro,
  ecto_repos: [Brasileiro.Repo]

# Configures the endpoint
config :brasileiro, BrasileiroWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "JgCtjlHLv+ULzevH9trsHrDlDZjxWDN9DsprCfgYsleM8Yb5ujvJw5JrfF4bHeb4",
  render_errors: [view: BrasileiroWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Brasileiro.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
