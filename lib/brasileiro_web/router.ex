defmodule BrasileiroWeb.Router do
  use BrasileiroWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BrasileiroWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/times", TimeController
    resources "/funcionarios", FuncionarioController
    resources "/resultados", ResultadoController
    resources "/jogadores", JogadorController
    resources "/escalacoes", EscalacaoController
    resources "/sumulas", SumulaController
    resources "/competicoes", CompeticaoController
    resources "/jogos", JogoController

  end

  # Other scopes may use custom stacks.
  # scope "/api", BrasileiroWeb do
  #   pipe_through :api
  # end
end
