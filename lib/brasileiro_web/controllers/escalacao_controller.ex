defmodule BrasileiroWeb.EscalacaoController do
  use BrasileiroWeb, :controller

  alias Brasileiro.Escalacoes
  alias Brasileiro.Escalacoes.Escalacao

  def index(conn, _params) do
    escalacoes = Escalacoes.list_escalacoes()
    render(conn, "index.html", escalacoes: escalacoes)
  end

  def new(conn, _params) do
    changeset = Escalacoes.change_escalacao(%Escalacao{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"escalacao" => escalacao_params}) do
    case Escalacoes.create_escalacao(escalacao_params) do
      {:ok, escalacao} ->
        conn
        |> put_flash(:info, "Escalacao created successfully.")
        |> redirect(to: escalacao_path(conn, :show, escalacao))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    escalacao = Escalacoes.get_escalacao!(id)
    render(conn, "show.html", escalacao: escalacao)
  end

  def edit(conn, %{"id" => id}) do
    escalacao = Escalacoes.get_escalacao!(id)
    changeset = Escalacoes.change_escalacao(escalacao)
    render(conn, "edit.html", escalacao: escalacao, changeset: changeset)
  end

  def update(conn, %{"id" => id, "escalacao" => escalacao_params}) do
    escalacao = Escalacoes.get_escalacao!(id)

    case Escalacoes.update_escalacao(escalacao, escalacao_params) do
      {:ok, escalacao} ->
        conn
        |> put_flash(:info, "Escalacao updated successfully.")
        |> redirect(to: escalacao_path(conn, :show, escalacao))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", escalacao: escalacao, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    escalacao = Escalacoes.get_escalacao!(id)
    {:ok, _escalacao} = Escalacoes.delete_escalacao(escalacao)

    conn
    |> put_flash(:info, "Escalacao deleted successfully.")
    |> redirect(to: escalacao_path(conn, :index))
  end
end
