defmodule BrasileiroWeb.CompeticaoController do
  use BrasileiroWeb, :controller

  alias Brasileiro.Competicoes
  alias Brasileiro.Competicoes.Competicao

  def index(conn, _params) do
    competicoes = Competicoes.list_competicoes()
    render(conn, "index.html", competicoes: competicoes)
  end

  def new(conn, _params) do
    changeset = Competicoes.change_competicao(%Competicao{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"competicao" => competicao_params}) do
    case Competicoes.create_competicao(competicao_params) do
      {:ok, competicao} ->
        conn
        |> put_flash(:info, "Competicao created successfully.")
        |> redirect(to: competicao_path(conn, :show, competicao))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    competicao = Competicoes.get_competicao!(id)
    render(conn, "show.html", competicao: competicao)
  end

  def edit(conn, %{"id" => id}) do
    competicao = Competicoes.get_competicao!(id)
    changeset = Competicoes.change_competicao(competicao)
    render(conn, "edit.html", competicao: competicao, changeset: changeset)
  end

  def update(conn, %{"id" => id, "competicao" => competicao_params}) do
    competicao = Competicoes.get_competicao!(id)

    case Competicoes.update_competicao(competicao, competicao_params) do
      {:ok, competicao} ->
        conn
        |> put_flash(:info, "Competicao updated successfully.")
        |> redirect(to: competicao_path(conn, :show, competicao))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", competicao: competicao, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    competicao = Competicoes.get_competicao!(id)
    {:ok, _competicao} = Competicoes.delete_competicao(competicao)

    conn
    |> put_flash(:info, "Competicao deleted successfully.")
    |> redirect(to: competicao_path(conn, :index))
  end
end
