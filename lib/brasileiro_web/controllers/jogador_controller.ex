defmodule BrasileiroWeb.JogadorController do
  use BrasileiroWeb, :controller

  alias Brasileiro.Jogadores
  alias Brasileiro.Jogadores.Jogador

  def index(conn, _params) do
    jogadores = Jogadores.list_jogadores()
    render(conn, "index.html", jogadores: jogadores)
  end

  def new(conn, _params) do
    changeset = Jogadores.change_jogador(%Jogador{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"jogador" => jogador_params}) do
    case Jogadores.create_jogador(jogador_params) do
      {:ok, jogador} ->
        conn
        |> put_flash(:info, "Jogador created successfully.")
        |> redirect(to: jogador_path(conn, :show, jogador))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    jogador = Jogadores.get_jogador!(id)
    render(conn, "show.html", jogador: jogador)
  end

  def edit(conn, %{"id" => id}) do
    jogador = Jogadores.get_jogador!(id)
    changeset = Jogadores.change_jogador(jogador)
    render(conn, "edit.html", jogador: jogador, changeset: changeset)
  end

  def update(conn, %{"id" => id, "jogador" => jogador_params}) do
    jogador = Jogadores.get_jogador!(id)

    case Jogadores.update_jogador(jogador, jogador_params) do
      {:ok, jogador} ->
        conn
        |> put_flash(:info, "Jogador updated successfully.")
        |> redirect(to: jogador_path(conn, :show, jogador))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", jogador: jogador, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    jogador = Jogadores.get_jogador!(id)
    {:ok, _jogador} = Jogadores.delete_jogador(jogador)

    conn
    |> put_flash(:info, "Jogador deleted successfully.")
    |> redirect(to: jogador_path(conn, :index))
  end
end
