defmodule BrasileiroWeb.JogoController do
  use BrasileiroWeb, :controller

  alias Brasileiro.Jogos
  alias Brasileiro.Jogos.Jogo

  def index(conn, _params) do
    jogos = Jogos.list_jogos()
    render(conn, "index.html", jogos: jogos)
  end

  def new(conn, _params) do
    changeset = Jogos.change_jogo(%Jogo{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"jogo" => jogo_params}) do
    case Jogos.create_jogo(jogo_params) do
      {:ok, jogo} ->
        conn
        |> put_flash(:info, "Jogo created successfully.")
        |> redirect(to: jogo_path(conn, :show, jogo))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    jogo = Jogos.get_jogo!(id)
    render(conn, "show.html", jogo: jogo)
  end

  def edit(conn, %{"id" => id}) do
    jogo = Jogos.get_jogo!(id)
    changeset = Jogos.change_jogo(jogo)
    render(conn, "edit.html", jogo: jogo, changeset: changeset)
  end

  def update(conn, %{"id" => id, "jogo" => jogo_params}) do
    jogo = Jogos.get_jogo!(id)

    case Jogos.update_jogo(jogo, jogo_params) do
      {:ok, jogo} ->
        conn
        |> put_flash(:info, "Jogo updated successfully.")
        |> redirect(to: jogo_path(conn, :show, jogo))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", jogo: jogo, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    jogo = Jogos.get_jogo!(id)
    {:ok, _jogo} = Jogos.delete_jogo(jogo)

    conn
    |> put_flash(:info, "Jogo deleted successfully.")
    |> redirect(to: jogo_path(conn, :index))
  end
end
