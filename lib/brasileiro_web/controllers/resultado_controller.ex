defmodule BrasileiroWeb.ResultadoController do
  use BrasileiroWeb, :controller

  alias Brasileiro.Resultados
  alias Brasileiro.Resultados.Resultado

  def index(conn, _params) do
    resultados = Resultados.list_resultados()
    render(conn, "index.html", resultados: resultados)
  end

  def new(conn, _params) do
    changeset = Resultados.change_resultado(%Resultado{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"resultado" => resultado_params}) do
    case Resultados.create_resultado(resultado_params) do
      {:ok, resultado} ->
        conn
        |> put_flash(:info, "Resultado created successfully.")
        |> redirect(to: resultado_path(conn, :show, resultado))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    resultado = Resultados.get_resultado!(id)
    render(conn, "show.html", resultado: resultado)
  end

  def edit(conn, %{"id" => id}) do
    resultado = Resultados.get_resultado!(id)
    changeset = Resultados.change_resultado(resultado)
    render(conn, "edit.html", resultado: resultado, changeset: changeset)
  end

  def update(conn, %{"id" => id, "resultado" => resultado_params}) do
    resultado = Resultados.get_resultado!(id)

    case Resultados.update_resultado(resultado, resultado_params) do
      {:ok, resultado} ->
        conn
        |> put_flash(:info, "Resultado updated successfully.")
        |> redirect(to: resultado_path(conn, :show, resultado))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", resultado: resultado, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    resultado = Resultados.get_resultado!(id)
    {:ok, _resultado} = Resultados.delete_resultado(resultado)

    conn
    |> put_flash(:info, "Resultado deleted successfully.")
    |> redirect(to: resultado_path(conn, :index))
  end
end
