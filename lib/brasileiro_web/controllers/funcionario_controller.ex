defmodule BrasileiroWeb.FuncionarioController do
  use BrasileiroWeb, :controller

  alias Brasileiro.Funcionarios
  alias Brasileiro.Funcionarios.Funcionario

  def index(conn, _params) do
    funcionarios = Funcionarios.list_funcionarios()
    render(conn, "index.html", funcionarios: funcionarios)
  end

  def new(conn, _params) do
    changeset = Funcionarios.change_funcionario(%Funcionario{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"funcionario" => funcionario_params}) do
    case Funcionarios.create_funcionario(funcionario_params) do
      {:ok, funcionario} ->
        conn
        |> put_flash(:info, "Funcionario created successfully.")
        |> redirect(to: funcionario_path(conn, :show, funcionario))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    funcionario = Funcionarios.get_funcionario!(id)
    render(conn, "show.html", funcionario: funcionario)
  end

  def edit(conn, %{"id" => id}) do
    funcionario = Funcionarios.get_funcionario!(id)
    changeset = Funcionarios.change_funcionario(funcionario)
    render(conn, "edit.html", funcionario: funcionario, changeset: changeset)
  end

  def update(conn, %{"id" => id, "funcionario" => funcionario_params}) do
    funcionario = Funcionarios.get_funcionario!(id)

    case Funcionarios.update_funcionario(funcionario, funcionario_params) do
      {:ok, funcionario} ->
        conn
        |> put_flash(:info, "Funcionario updated successfully.")
        |> redirect(to: funcionario_path(conn, :show, funcionario))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", funcionario: funcionario, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    funcionario = Funcionarios.get_funcionario!(id)
    {:ok, _funcionario} = Funcionarios.delete_funcionario(funcionario)

    conn
    |> put_flash(:info, "Funcionario deleted successfully.")
    |> redirect(to: funcionario_path(conn, :index))
  end
end
