defmodule BrasileiroWeb.SumulaController do
  use BrasileiroWeb, :controller

  alias Brasileiro.Sumulas
  alias Brasileiro.Sumulas.Sumula

  def index(conn, _params) do
    sumulas = Sumulas.list_sumulas()
    render(conn, "index.html", sumulas: sumulas)
  end

  def new(conn, _params) do
    changeset = Sumulas.change_sumula(%Sumula{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"sumula" => sumula_params}) do
    case Sumulas.create_sumula(sumula_params) do
      {:ok, sumula} ->
        conn
        |> put_flash(:info, "Sumula created successfully.")
        |> redirect(to: sumula_path(conn, :show, sumula))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    sumula = Sumulas.get_sumula!(id)
    render(conn, "show.html", sumula: sumula)
  end

  def edit(conn, %{"id" => id}) do
    sumula = Sumulas.get_sumula!(id)
    changeset = Sumulas.change_sumula(sumula)
    render(conn, "edit.html", sumula: sumula, changeset: changeset)
  end

  def update(conn, %{"id" => id, "sumula" => sumula_params}) do
    sumula = Sumulas.get_sumula!(id)

    case Sumulas.update_sumula(sumula, sumula_params) do
      {:ok, sumula} ->
        conn
        |> put_flash(:info, "Sumula updated successfully.")
        |> redirect(to: sumula_path(conn, :show, sumula))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", sumula: sumula, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    sumula = Sumulas.get_sumula!(id)
    {:ok, _sumula} = Sumulas.delete_sumula(sumula)

    conn
    |> put_flash(:info, "Sumula deleted successfully.")
    |> redirect(to: sumula_path(conn, :index))
  end
end
