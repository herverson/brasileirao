defmodule Brasileiro.Funcionarios.Funcionario do
  use Ecto.Schema
  import Ecto.Changeset


  schema "funcionarios" do
    field :contato, :string
    field :nome, :string
    field :profissao, :string

    timestamps()
  end

  @doc false
  def changeset(funcionario, attrs) do
    funcionario
    |> cast(attrs, [:nome, :profissao, :contato])
    |> validate_required([:nome, :profissao, :contato])
  end
end
