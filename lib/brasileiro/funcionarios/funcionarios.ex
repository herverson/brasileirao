defmodule Brasileiro.Funcionarios do
  @moduledoc """
  The Funcionarios context.
  """

  import Ecto.Query, warn: false
  alias Brasileiro.Repo

  alias Brasileiro.Funcionarios.Funcionario

  @doc """
  Returns the list of funcionarios.

  ## Examples

      iex> list_funcionarios()
      [%Funcionario{}, ...]

  """
  def list_funcionarios do
    Repo.all(Funcionario)
  end

  @doc """
  Gets a single funcionario.

  Raises `Ecto.NoResultsError` if the Funcionario does not exist.

  ## Examples

      iex> get_funcionario!(123)
      %Funcionario{}

      iex> get_funcionario!(456)
      ** (Ecto.NoResultsError)

  """
  def get_funcionario!(id), do: Repo.get!(Funcionario, id)

  @doc """
  Creates a funcionario.

  ## Examples

      iex> create_funcionario(%{field: value})
      {:ok, %Funcionario{}}

      iex> create_funcionario(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_funcionario(attrs \\ %{}) do
    %Funcionario{}
    |> Funcionario.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a funcionario.

  ## Examples

      iex> update_funcionario(funcionario, %{field: new_value})
      {:ok, %Funcionario{}}

      iex> update_funcionario(funcionario, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_funcionario(%Funcionario{} = funcionario, attrs) do
    funcionario
    |> Funcionario.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Funcionario.

  ## Examples

      iex> delete_funcionario(funcionario)
      {:ok, %Funcionario{}}

      iex> delete_funcionario(funcionario)
      {:error, %Ecto.Changeset{}}

  """
  def delete_funcionario(%Funcionario{} = funcionario) do
    Repo.delete(funcionario)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking funcionario changes.

  ## Examples

      iex> change_funcionario(funcionario)
      %Ecto.Changeset{source: %Funcionario{}}

  """
  def change_funcionario(%Funcionario{} = funcionario) do
    Funcionario.changeset(funcionario, %{})
  end
end
