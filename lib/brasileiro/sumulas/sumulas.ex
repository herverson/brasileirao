defmodule Brasileiro.Sumulas do
  @moduledoc """
  The Sumulas context.
  """

  import Ecto.Query, warn: false
  alias Brasileiro.Repo

  alias Brasileiro.Sumulas.Sumula

  @doc """
  Returns the list of sumulas.

  ## Examples

      iex> list_sumulas()
      [%Sumula{}, ...]

  """
  def list_sumulas do
    Repo.all(Sumula)
  end

  @doc """
  Gets a single sumula.

  Raises `Ecto.NoResultsError` if the Sumula does not exist.

  ## Examples

      iex> get_sumula!(123)
      %Sumula{}

      iex> get_sumula!(456)
      ** (Ecto.NoResultsError)

  """
  def get_sumula!(id), do: Repo.get!(Sumula, id)

  @doc """
  Creates a sumula.

  ## Examples

      iex> create_sumula(%{field: value})
      {:ok, %Sumula{}}

      iex> create_sumula(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_sumula(attrs \\ %{}) do
    %Sumula{}
    |> Sumula.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a sumula.

  ## Examples

      iex> update_sumula(sumula, %{field: new_value})
      {:ok, %Sumula{}}

      iex> update_sumula(sumula, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_sumula(%Sumula{} = sumula, attrs) do
    sumula
    |> Sumula.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Sumula.

  ## Examples

      iex> delete_sumula(sumula)
      {:ok, %Sumula{}}

      iex> delete_sumula(sumula)
      {:error, %Ecto.Changeset{}}

  """
  def delete_sumula(%Sumula{} = sumula) do
    Repo.delete(sumula)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking sumula changes.

  ## Examples

      iex> change_sumula(sumula)
      %Ecto.Changeset{source: %Sumula{}}

  """
  def change_sumula(%Sumula{} = sumula) do
    Sumula.changeset(sumula, %{})
  end
end
