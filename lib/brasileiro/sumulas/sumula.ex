defmodule Brasileiro.Sumulas.Sumula do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sumulas" do
    field :id_jogador_amarelo, :integer
    field :id_jogador_vermelho, :integer
    field :relatorio, :string

    timestamps()
  end

  @doc false
  def changeset(sumula, attrs) do
    sumula
    |> cast(attrs, [:id_jogador_amarelo, :id_jogador_vermelho, :relatorio])
    |> validate_required([:id_jogador_amarelo, :id_jogador_vermelho, :relatorio])
  end
end
