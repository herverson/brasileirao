defmodule Brasileiro.Jogadores do
  @moduledoc """
  The Jogadores context.
  """

  import Ecto.Query, warn: false
  alias Brasileiro.Repo

  alias Brasileiro.Jogadores.Jogador

  @doc """
  Returns the list of jogadores.

  ## Examples

      iex> list_jogadores()
      [%Jogador{}, ...]

  """
  def list_jogadores do
    Repo.all(Jogador)
  end

  @doc """
  Gets a single jogador.

  Raises `Ecto.NoResultsError` if the Jogador does not exist.

  ## Examples

      iex> get_jogador!(123)
      %Jogador{}

      iex> get_jogador!(456)
      ** (Ecto.NoResultsError)

  """
  def get_jogador!(id), do: Repo.get!(Jogador, id)

  @doc """
  Creates a jogador.

  ## Examples

      iex> create_jogador(%{field: value})
      {:ok, %Jogador{}}

      iex> create_jogador(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_jogador(attrs \\ %{}) do
    %Jogador{}
    |> Jogador.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a jogador.

  ## Examples

      iex> update_jogador(jogador, %{field: new_value})
      {:ok, %Jogador{}}

      iex> update_jogador(jogador, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_jogador(%Jogador{} = jogador, attrs) do
    jogador
    |> Jogador.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Jogador.

  ## Examples

      iex> delete_jogador(jogador)
      {:ok, %Jogador{}}

      iex> delete_jogador(jogador)
      {:error, %Ecto.Changeset{}}

  """
  def delete_jogador(%Jogador{} = jogador) do
    Repo.delete(jogador)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking jogador changes.

  ## Examples

      iex> change_jogador(jogador)
      %Ecto.Changeset{source: %Jogador{}}

  """
  def change_jogador(%Jogador{} = jogador) do
    Jogador.changeset(jogador, %{})
  end
end
