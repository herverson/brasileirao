defmodule Brasileiro.Jogadores.Jogador do
  use Ecto.Schema
  import Ecto.Changeset


  schema "jogadores" do
    field :nome, :string
    field :numero_camisa, :string
    field :posicao, :string

    timestamps()
  end

  @doc false
  def changeset(jogador, attrs) do
    jogador
    |> cast(attrs, [:nome, :posicao, :numero_camisa])
    |> validate_required([:nome, :posicao, :numero_camisa])
  end
end
