defmodule Brasileiro.Jogos.Jogo do
  use Ecto.Schema
  import Ecto.Changeset


  schema "jogos" do
    field :comissao_tecnica, :integer
    field :data, :naive_datetime
    field :id_escalacao_casa, :integer
    field :id_escalacao_visitante, :integer
    field :id_resultado, :integer
    field :local, :string
    field :sumula, :integer

    timestamps()
  end

  @doc false
  def changeset(jogo, attrs) do
    jogo
    |> cast(attrs, [:data, :id_escalacao_casa, :id_escalacao_visitante, :id_resultado, :comissao_tecnica, :sumula, :local])
    |> validate_required([:data, :id_escalacao_casa, :id_escalacao_visitante, :id_resultado, :comissao_tecnica, :sumula, :local])
  end
end
