defmodule Brasileiro.Jogos do
  @moduledoc """
  The Jogos context.
  """

  import Ecto.Query, warn: false
  alias Brasileiro.Repo

  alias Brasileiro.Jogos.Jogo

  @doc """
  Returns the list of jogos.

  ## Examples

      iex> list_jogos()
      [%Jogo{}, ...]

  """
  def list_jogos do
    Repo.all(Jogo)
  end

  @doc """
  Gets a single jogo.

  Raises `Ecto.NoResultsError` if the Jogo does not exist.

  ## Examples

      iex> get_jogo!(123)
      %Jogo{}

      iex> get_jogo!(456)
      ** (Ecto.NoResultsError)

  """
  def get_jogo!(id), do: Repo.get!(Jogo, id)

  @doc """
  Creates a jogo.

  ## Examples

      iex> create_jogo(%{field: value})
      {:ok, %Jogo{}}

      iex> create_jogo(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_jogo(attrs \\ %{}) do
    %Jogo{}
    |> Jogo.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a jogo.

  ## Examples

      iex> update_jogo(jogo, %{field: new_value})
      {:ok, %Jogo{}}

      iex> update_jogo(jogo, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_jogo(%Jogo{} = jogo, attrs) do
    jogo
    |> Jogo.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Jogo.

  ## Examples

      iex> delete_jogo(jogo)
      {:ok, %Jogo{}}

      iex> delete_jogo(jogo)
      {:error, %Ecto.Changeset{}}

  """
  def delete_jogo(%Jogo{} = jogo) do
    Repo.delete(jogo)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking jogo changes.

  ## Examples

      iex> change_jogo(jogo)
      %Ecto.Changeset{source: %Jogo{}}

  """
  def change_jogo(%Jogo{} = jogo) do
    Jogo.changeset(jogo, %{})
  end
end
