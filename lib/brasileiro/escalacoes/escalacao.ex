defmodule Brasileiro.Escalacoes.Escalacao do
  use Ecto.Schema
  import Ecto.Changeset


  schema "escalacoes" do
    field :id_time, :integer
    field :jogadores_principais, :integer
    field :jogadores_reservas, :integer
    field :tecnico, :integer

    timestamps()
  end

  @doc false
  def changeset(escalacao, attrs) do
    escalacao
    |> cast(attrs, [:id_time, :jogadores_principais, :jogadores_reservas, :tecnico])
    |> validate_required([:id_time, :jogadores_principais, :jogadores_reservas, :tecnico])
  end
end
