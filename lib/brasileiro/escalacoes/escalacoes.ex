defmodule Brasileiro.Escalacoes do
  @moduledoc """
  The Escalacoes context.
  """

  import Ecto.Query, warn: false
  alias Brasileiro.Repo

  alias Brasileiro.Escalacoes.Escalacao

  @doc """
  Returns the list of escalacoes.

  ## Examples

      iex> list_escalacoes()
      [%Escalacao{}, ...]

  """
  def list_escalacoes do
    Repo.all(Escalacao)
  end

  @doc """
  Gets a single escalacao.

  Raises `Ecto.NoResultsError` if the Escalacao does not exist.

  ## Examples

      iex> get_escalacao!(123)
      %Escalacao{}

      iex> get_escalacao!(456)
      ** (Ecto.NoResultsError)

  """
  def get_escalacao!(id), do: Repo.get!(Escalacao, id)

  @doc """
  Creates a escalacao.

  ## Examples

      iex> create_escalacao(%{field: value})
      {:ok, %Escalacao{}}

      iex> create_escalacao(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_escalacao(attrs \\ %{}) do
    %Escalacao{}
    |> Escalacao.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a escalacao.

  ## Examples

      iex> update_escalacao(escalacao, %{field: new_value})
      {:ok, %Escalacao{}}

      iex> update_escalacao(escalacao, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_escalacao(%Escalacao{} = escalacao, attrs) do
    escalacao
    |> Escalacao.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Escalacao.

  ## Examples

      iex> delete_escalacao(escalacao)
      {:ok, %Escalacao{}}

      iex> delete_escalacao(escalacao)
      {:error, %Ecto.Changeset{}}

  """
  def delete_escalacao(%Escalacao{} = escalacao) do
    Repo.delete(escalacao)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking escalacao changes.

  ## Examples

      iex> change_escalacao(escalacao)
      %Ecto.Changeset{source: %Escalacao{}}

  """
  def change_escalacao(%Escalacao{} = escalacao) do
    Escalacao.changeset(escalacao, %{})
  end
end
