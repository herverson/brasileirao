defmodule Brasileiro.Resultados.Resultado do
  use Ecto.Schema
  import Ecto.Changeset


  schema "resultados" do
    field :gols_casa, :integer
    field :gols_visitante, :integer
    field :id_perdedor, :integer
    field :id_vencedor, :integer

    timestamps()
  end

  @doc false
  def changeset(resultado, attrs) do
    resultado
    |> cast(attrs, [:gols_visitante, :gols_casa, :id_vencedor, :id_perdedor])
    |> validate_required([:gols_visitante, :gols_casa, :id_vencedor, :id_perdedor])
  end
end
