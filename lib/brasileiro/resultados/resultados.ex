defmodule Brasileiro.Resultados do
  @moduledoc """
  The Resultados context.
  """

  import Ecto.Query, warn: false
  alias Brasileiro.Repo

  alias Brasileiro.Resultados.Resultado

  @doc """
  Returns the list of resultados.

  ## Examples

      iex> list_resultados()
      [%Resultado{}, ...]

  """
  def list_resultados do
    Repo.all(Resultado)
  end

  @doc """
  Gets a single resultado.

  Raises `Ecto.NoResultsError` if the Resultado does not exist.

  ## Examples

      iex> get_resultado!(123)
      %Resultado{}

      iex> get_resultado!(456)
      ** (Ecto.NoResultsError)

  """
  def get_resultado!(id), do: Repo.get!(Resultado, id)

  @doc """
  Creates a resultado.

  ## Examples

      iex> create_resultado(%{field: value})
      {:ok, %Resultado{}}

      iex> create_resultado(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_resultado(attrs \\ %{}) do
    %Resultado{}
    |> Resultado.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a resultado.

  ## Examples

      iex> update_resultado(resultado, %{field: new_value})
      {:ok, %Resultado{}}

      iex> update_resultado(resultado, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_resultado(%Resultado{} = resultado, attrs) do
    resultado
    |> Resultado.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Resultado.

  ## Examples

      iex> delete_resultado(resultado)
      {:ok, %Resultado{}}

      iex> delete_resultado(resultado)
      {:error, %Ecto.Changeset{}}

  """
  def delete_resultado(%Resultado{} = resultado) do
    Repo.delete(resultado)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking resultado changes.

  ## Examples

      iex> change_resultado(resultado)
      %Ecto.Changeset{source: %Resultado{}}

  """
  def change_resultado(%Resultado{} = resultado) do
    Resultado.changeset(resultado, %{})
  end
end
