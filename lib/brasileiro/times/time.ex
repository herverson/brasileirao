defmodule Brasileiro.Times.Time do
  use Ecto.Schema
  import Ecto.Changeset


  schema "times" do
    field :comissao_tecnica, :integer
    field :id_jogador, :integer
    field :nome, :string

    timestamps()
  end

  @doc false
  def changeset(time, attrs) do
    time
    |> cast(attrs, [:nome, :id_jogador, :comissao_tecnica])
    |> validate_required([:nome, :id_jogador, :comissao_tecnica])
  end
end
