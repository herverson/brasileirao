defmodule Brasileiro.Competicoes.Competicao do
  use Ecto.Schema
  import Ecto.Changeset


  schema "competicoes" do
    field :id_jogo, :integer
    field :id_time, :integer
    field :nome, :string

    timestamps()
  end

  @doc false
  def changeset(competicao, attrs) do
    competicao
    |> cast(attrs, [:nome, :id_time, :id_jogo])
    |> validate_required([:nome, :id_time, :id_jogo])
  end
end
