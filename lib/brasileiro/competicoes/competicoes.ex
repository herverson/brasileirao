defmodule Brasileiro.Competicoes do
  @moduledoc """
  The Competicoes context.
  """

  import Ecto.Query, warn: false
  alias Brasileiro.Repo

  alias Brasileiro.Competicoes.Competicao

  @doc """
  Returns the list of competicoes.

  ## Examples

      iex> list_competicoes()
      [%Competicao{}, ...]

  """
  def list_competicoes do
    Repo.all(Competicao)
  end

  @doc """
  Gets a single competicao.

  Raises `Ecto.NoResultsError` if the Competicao does not exist.

  ## Examples

      iex> get_competicao!(123)
      %Competicao{}

      iex> get_competicao!(456)
      ** (Ecto.NoResultsError)

  """
  def get_competicao!(id), do: Repo.get!(Competicao, id)

  @doc """
  Creates a competicao.

  ## Examples

      iex> create_competicao(%{field: value})
      {:ok, %Competicao{}}

      iex> create_competicao(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_competicao(attrs \\ %{}) do
    %Competicao{}
    |> Competicao.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a competicao.

  ## Examples

      iex> update_competicao(competicao, %{field: new_value})
      {:ok, %Competicao{}}

      iex> update_competicao(competicao, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_competicao(%Competicao{} = competicao, attrs) do
    competicao
    |> Competicao.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Competicao.

  ## Examples

      iex> delete_competicao(competicao)
      {:ok, %Competicao{}}

      iex> delete_competicao(competicao)
      {:error, %Ecto.Changeset{}}

  """
  def delete_competicao(%Competicao{} = competicao) do
    Repo.delete(competicao)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking competicao changes.

  ## Examples

      iex> change_competicao(competicao)
      %Ecto.Changeset{source: %Competicao{}}

  """
  def change_competicao(%Competicao{} = competicao) do
    Competicao.changeset(competicao, %{})
  end
end
