defmodule Brasileiro.Repo.Migrations.CreateResultados do
  use Ecto.Migration

  def change do
    create table(:resultados) do
      add :gols_visitante, :integer
      add :gols_casa, :integer
      add :id_vencedor, :integer
      add :id_perdedor, :integer

      timestamps()
    end

  end
end
