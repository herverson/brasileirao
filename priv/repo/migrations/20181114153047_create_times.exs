defmodule Brasileiro.Repo.Migrations.CreateTimes do
  use Ecto.Migration

  def change do
    create table(:times) do
      add :nome, :string
      add :id_jogador, :integer
      add :comissao_tecnica, :integer

      timestamps()
    end

  end
end
