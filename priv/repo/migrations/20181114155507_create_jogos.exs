defmodule Brasileiro.Repo.Migrations.CreateJogos do
  use Ecto.Migration

  def change do
    create table(:jogos) do
      add :data, :naive_datetime
      add :id_escalacao_casa, :integer
      add :id_escalacao_visitante, :integer
      add :id_resultado, :integer
      add :comissao_tecnica, :integer
      add :sumula, :integer
      add :local, :string

      timestamps()
    end

  end
end
