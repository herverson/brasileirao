defmodule Brasileiro.Repo.Migrations.CreateCompeticoes do
  use Ecto.Migration

  def change do
    create table(:competicoes) do
      add :nome, :string
      add :id_time, :integer
      add :id_jogo, :integer

      timestamps()
    end

  end
end
