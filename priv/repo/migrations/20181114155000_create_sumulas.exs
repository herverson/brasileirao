defmodule Brasileiro.Repo.Migrations.CreateSumulas do
  use Ecto.Migration

  def change do
    create table(:sumulas) do
      add :id_jogador_amarelo, :integer
      add :id_jogador_vermelho, :integer
      add :relatorio, :string

      timestamps()
    end

  end
end
