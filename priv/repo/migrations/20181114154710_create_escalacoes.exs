defmodule Brasileiro.Repo.Migrations.CreateEscalacoes do
  use Ecto.Migration

  def change do
    create table(:escalacoes) do
      add :id_time, :integer
      add :jogadores_principais, :integer
      add :jogadores_reservas, :integer
      add :tecnico, :integer

      timestamps()
    end

  end
end
