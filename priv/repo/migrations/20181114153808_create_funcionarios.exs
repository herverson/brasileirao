defmodule Brasileiro.Repo.Migrations.CreateFuncionarios do
  use Ecto.Migration

  def change do
    create table(:funcionarios) do
      add :nome, :string
      add :profissao, :string
      add :contato, :string

      timestamps()
    end

  end
end
