defmodule Brasileiro.Repo.Migrations.CreateJogadores do
  use Ecto.Migration

  def change do
    create table(:jogadores) do
      add :nome, :string
      add :posicao, :string
      add :numero_camisa, :string

      timestamps()
    end

  end
end
